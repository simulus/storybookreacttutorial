// -*-; mode: js2-jsx; -*-

import React from 'react';

export type TaskState = 'TASK_INBOX' | 'TASK_PINNED' | 'TASK_ARCHIVED'; 

export interface TaskInterface {
  id: number;
  title: string;
  state: TaskState;
}

export default function Task({ task: { id, title, state }, onArchiveTask, onPinTask }) {
  return (
    <div className="list-item">
      <input type="text" value={title} readOnly={true} />
    </div>
  );
}
